import Bot from './bot';

class WeatherBot extends Bot {
  constructor(name, avatar) {
    super(name, avatar);
    this.addAction('weather', this.getWeather);
  }

  async getWeather() {
    try {
      const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=48.1173&longitude=-1.6778&hourly=temperature_2m');
      const data = await response.json();
      return `Current temperature in Rennes: ${data.hourly.temperature_2m[0]}°C`;
    } catch (error) {
      return 'Failed to fetch weather data';
    }
  }
}

export default WeatherBot;
